package com.example.fonyou_test.data.repository;

import com.example.fonyou_test.data.models.Respuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RespuestasRepository extends JpaRepository<Respuesta, Integer> {

}
