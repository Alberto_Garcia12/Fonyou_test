package com.example.fonyou_test.data.payloads.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RespuestasRequest {

    @NotBlank
    @NotNull
    private Integer foreingKey_Estudiante;

    @NotBlank
    @NotNull
    private String codigoPregunta;

    @NotBlank
    @NotNull
    private String respuestas;

    public Integer getForeingKey_Estudiante() {
        return foreingKey_Estudiante;
    }

    public void setForeingKey_Estudiante(Integer foreingKey_Estudiante) {
        this.foreingKey_Estudiante = foreingKey_Estudiante;
    }

    public String getCodigoPregunta() {
        return codigoPregunta;
    }

    public void setCodigoPregunta(String codigoPregunta) {
        this.codigoPregunta = codigoPregunta;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }
}
