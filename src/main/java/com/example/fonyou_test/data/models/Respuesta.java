package com.example.fonyou_test.data.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Respuesta {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * params para Respuestas
     * OneToOne
     */
    private Integer foreingKey_Estudiante;

    /**
     * params para Respuestas
     */
    private String codigoPregunta;

    /**
     * params para Respuestas
     */
    private String respuestas;


    public Respuesta() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getForeingKey_Estudiante() {
        return foreingKey_Estudiante;
    }

    public void setForeingKey_Estudiante(Integer foreingKey_Estudiante) {
        this.foreingKey_Estudiante = foreingKey_Estudiante;
    }

    public String getCodigoPregunta() {
        return codigoPregunta;
    }

    public void setCodigoPregunta(String codigoPregunta) {
        this.codigoPregunta = codigoPregunta;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Respuesta respuesta = (Respuesta) o;
        return id.equals(respuesta.id) && foreingKey_Estudiante.equals(respuesta.foreingKey_Estudiante) && codigoPregunta.equals(respuesta.codigoPregunta) && respuestas.equals(respuesta.respuestas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, foreingKey_Estudiante, codigoPregunta, respuestas);
    }

    @Override
    public String toString() {
        return "Respuesta{" +
                "id=" + id +
                ", foreingKey_Estudiante=" + foreingKey_Estudiante +
                ", codigoPregunta='" + codigoPregunta + '\'' +
                ", respuestas='" + respuestas + '\'' +
                '}';
    }
}
