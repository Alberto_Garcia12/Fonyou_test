package com.example.fonyou_test.data.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Examen {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * params para Examen
     */
    private String codigoPregunta;

    /**
     * params para Examen
     */
    private String pregunta;

    /**
     * params para Examen
     */
    private String respuesta1;

    /**
     * params para Examen
     */
    private String respuesta2;

    /**
     * params para Examen
     */
    private String respuesta3;

    /**
     * params para Examen
     */
    private String respuesta4;

    /**
     * params para Examen
     */
    private String respuestaCorrecta;

    public Examen() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigoPregunta() {
        return codigoPregunta;
    }

    public void setCodigoPregunta(String codigoPregunta) {
        this.codigoPregunta = codigoPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta1() {
        return respuesta1;
    }

    public void setRespuesta1(String respuesta1) {
        this.respuesta1 = respuesta1;
    }

    public String getRespuesta2() {
        return respuesta2;
    }

    public void setRespuesta2(String respuesta2) {
        this.respuesta2 = respuesta2;
    }

    public String getRespuesta3() {
        return respuesta3;
    }

    public void setRespuesta3(String respuesta3) {
        this.respuesta3 = respuesta3;
    }

    public String getRespuesta4() {
        return respuesta4;
    }

    public void setRespuesta4(String respuesta4) {
        this.respuesta4 = respuesta4;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    @Override
    public String toString() {
        return "Examen{" +
                "id=" + id +
                ", codigoPregunta='" + codigoPregunta + '\'' +
                ", pregunta='" + pregunta + '\'' +
                ", respuesta1='" + respuesta1 + '\'' +
                ", respuesta2='" + respuesta2 + '\'' +
                ", respuesta3='" + respuesta3 + '\'' +
                ", respuesta4='" + respuesta4 + '\'' +
                ", respuestaCorrecta='" + respuestaCorrecta + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Examen examen = (Examen) o;
        return Objects.equals(id, examen.id) && Objects.equals(codigoPregunta, examen.codigoPregunta) &&
                Objects.equals(pregunta, examen.pregunta) && Objects.equals(respuesta1, examen.respuesta1) &&
                Objects.equals(respuesta2, examen.respuesta2) && Objects.equals(respuesta3, examen.respuesta3) &&
                Objects.equals(respuesta4, examen.respuesta4) && Objects.equals(respuestaCorrecta, examen.respuestaCorrecta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigoPregunta, pregunta, respuesta1, respuesta2, respuesta3, respuesta4, respuestaCorrecta);
    }
}
