package com.example.fonyou_test.data.models;

public enum Department {
    HUMAN_RESOURCE,
    MARKETING,
    ENGINEERING
}
