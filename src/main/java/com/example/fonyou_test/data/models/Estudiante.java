package com.example.fonyou_test.data.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Estudiante {
    /**
     * params para estudiante
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * params para estudiante
     */
    private String firstName;

    /**
     * params para estudiante
     */
    private String lastname;

    /**
     * params para estudiante
     */
    private String phoneNumber;

    /**
     * params para estudiante
     */
    private String email;

    /**
     * params para estudiante
     */
    private double salary;
    /**
     * params Enum
     */
    @Enumerated(EnumType.STRING)
    private Department department;

    public Estudiante() {
    }

    /**
     * Getter y setter
     */
    public Integer getId() {
        return id;
    }

    /**
     * Getter y setter
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter y setter
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Getter y setter
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter y setter
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Getter y setter
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Getter y setter
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Getter y setter
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Getter y setter
     */
    public String getEmail() {
        return email;
    }

    /**
     * Getter y setter
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter y setter
     */
    public double getSalary() {
        return salary;
    }

    /**
     * Getter y setter
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * Getter y setter
     */
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Estudiante{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastname='" + lastname + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", salary=" + salary +
                ", department=" + department +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Estudiante estudiante = (Estudiante) o;
        return Double.compare(estudiante.salary, salary) == 0 && Objects.equals(id, estudiante.id) &&
                Objects.equals(firstName, estudiante.firstName) && Objects.equals(lastname, estudiante.lastname) &&
                Objects.equals(phoneNumber, estudiante.phoneNumber) && Objects.equals(email, estudiante.email) && department == estudiante.department;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastname, phoneNumber, email, salary, department);
    }
}
