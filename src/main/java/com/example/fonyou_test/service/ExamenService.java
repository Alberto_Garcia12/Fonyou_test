package com.example.fonyou_test.service;

import com.example.fonyou_test.data.models.Examen;
import com.example.fonyou_test.data.payloads.request.ExamenRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ExamenService {

    MessageResponse createExamen(ExamenRequest examenRequest);

    Examen updateExamen(Integer examenId, ExamenRequest examenRequest);

    void deleteExamen(Integer examenId);

    Examen getExamenById(Integer examenId);

    List<Examen> getAllExamen();
}
