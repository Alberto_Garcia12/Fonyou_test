package com.example.fonyou_test.service;

import com.example.fonyou_test.data.models.Estudiante;
import com.example.fonyou_test.data.payloads.request.EstudianteRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import com.example.fonyou_test.data.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EstudianteServiceImpl implements EstudianteService {

    @Autowired
    EstudianteRepository estudianteRepository;

    @Override
    public MessageResponse createEstudiante(EstudianteRequest estudianteRequest) {
        Estudiante newEstudiante = new Estudiante();
        newEstudiante.setFirstName(estudianteRequest.getFirstName());
        newEstudiante.setLastname(estudianteRequest.getLastname());
        newEstudiante.setPhoneNumber(estudianteRequest.getPhoneNumber());
        newEstudiante.setEmail(estudianteRequest.getEmail());
        newEstudiante.setSalary(estudianteRequest.getSalary());
        newEstudiante.setDepartment(estudianteRequest.getDepartment());
        estudianteRepository.save(newEstudiante);
        return new MessageResponse("Nuevo Estudiante creado correctamente...");
    }

    @Override
    public Estudiante updateEstudiante(Integer estudianteId, EstudianteRequest estudianteRequest) {
        Optional<Estudiante> estudiante = estudianteRepository.findById(estudianteId);
        if (estudiante.isEmpty()) {
            return new Estudiante();
        } else {
            estudiante.get().setFirstName(estudianteRequest.getFirstName());
            estudiante.get().setLastname(estudianteRequest.getLastname());
            estudiante.get().setPhoneNumber(estudianteRequest.getPhoneNumber());
            estudiante.get().setEmail(estudianteRequest.getEmail());
            estudiante.get().setSalary(estudianteRequest.getSalary());
            estudiante.get().setDepartment(estudianteRequest.getDepartment());
            estudianteRepository.save(estudiante.get());
            return estudiante.get();
        }
    }

    @Override
    public void deleteEstudiante(Integer estudianteId) {
        if (estudianteRepository.getById(estudianteId).getId().equals(estudianteId)) {
            estudianteRepository.deleteById(estudianteId);
        }
        //else throw new ResourceNotFoundException("Estudiante", "id", estudianteId);
    }

    @Override
    public Estudiante getASingleEstudiante(Integer estudianteId) {
        return estudianteRepository.findById(estudianteId).orElseThrow(() -> null);
    }

    @Override
    public List<Estudiante> getAllEstudiante() {
        return estudianteRepository.findAll();
    }
}
