package com.example.fonyou_test.service;

import com.example.fonyou_test.data.models.Respuesta;
import com.example.fonyou_test.data.payloads.request.RespuestasRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import com.example.fonyou_test.data.repository.RespuestasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RespuestasServiceImpl implements RespuestasService {

    @Autowired
    RespuestasRepository respuestasRepository;

    /**
     * @param respuestasRequest
     * @return
     */
    @Override
    public MessageResponse createRespuestas(RespuestasRequest respuestasRequest) {
        Respuesta newRespuesta = new Respuesta();
        newRespuesta.setForeingKey_Estudiante(respuestasRequest.getForeingKey_Estudiante());
        newRespuesta.setCodigoPregunta(respuestasRequest.getCodigoPregunta());
        newRespuesta.setRespuestas(respuestasRequest.getRespuestas());
        respuestasRepository.save(newRespuesta);
        return new MessageResponse("Respuestas guardadas correctamente...");
    }

    /**
     * @param respuestasId
     * @param respuestasRequest
     * @return
     */
    @Override
    public Respuesta updateRespuestas(Integer respuestasId, RespuestasRequest respuestasRequest) {
        Optional<Respuesta> respuesta = respuestasRepository.findById(respuestasId);
        if (respuesta.isEmpty()) {
            return new Respuesta();
        } else {
            respuesta.get().setForeingKey_Estudiante(respuestasRequest.getForeingKey_Estudiante());
            respuesta.get().setCodigoPregunta(respuestasRequest.getCodigoPregunta());
            respuesta.get().setRespuestas(respuestasRequest.getRespuestas());
            respuestasRepository.save(respuesta.get());
            return respuesta.get();
        }
    }

    /**
     * @param respuestasId
     */
    @Override
    public void deleteRespuestas(Integer respuestasId) {
        if (respuestasRepository.getById(respuestasId).getId().equals(respuestasId)) {
            respuestasRepository.deleteById(respuestasId);
        }
    }

    /**
     * @param respuestasId
     * @return
     */
    @Override
    public Respuesta getASingleRespuestas(Integer respuestasId) {
        return respuestasRepository.findById(respuestasId).orElseThrow(() -> null);
    }

    /**
     * @return
     */
    @Override
    public List<Respuesta> getAllRespuestas() {
        return respuestasRepository.findAll();
    }
}
