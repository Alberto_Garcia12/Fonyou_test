package com.example.fonyou_test.service;

import com.example.fonyou_test.data.models.Examen;
import com.example.fonyou_test.data.payloads.request.ExamenRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import com.example.fonyou_test.data.repository.ExamenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExamenServiceImpl implements ExamenService {

    @Autowired
    ExamenRepository examenRepository;

    /**
     * @param examenRequest
     * @return
     */
    @Override
    public MessageResponse createExamen(ExamenRequest examenRequest) {
        Examen newExamen = new Examen();
        newExamen.setCodigoPregunta(examenRequest.getCodigoPregunta());
        newExamen.setPregunta(examenRequest.getPregunta());
        newExamen.setRespuesta1(examenRequest.getRespuesta1());
        newExamen.setRespuesta2(examenRequest.getRespuesta2());
        newExamen.setRespuesta3(examenRequest.getRespuesta3());
        newExamen.setRespuesta4(examenRequest.getRespuesta4());
        newExamen.setRespuestaCorrecta(examenRequest.getRespuestaCorrecta());
        examenRepository.save(newExamen);
        return new MessageResponse("Examen Agregado Correctamente...");
    }

    /**
     * @param examenId
     * @return
     */
    @Override
    public Examen updateExamen(Integer examenId, ExamenRequest examenRequest) {
        Optional<Examen> examen = examenRepository.findById(examenId);
        if (examen.isEmpty()) {
            return new Examen();
        } else {
            examen.get().setCodigoPregunta(examenRequest.getCodigoPregunta());
            examen.get().setPregunta(examenRequest.getPregunta());
            examen.get().setRespuesta1(examenRequest.getRespuesta1());
            examen.get().setRespuesta2(examenRequest.getRespuesta2());
            examen.get().setRespuesta3(examenRequest.getRespuesta3());
            examen.get().setRespuesta4(examenRequest.getRespuesta4());
            examen.get().setRespuestaCorrecta(examenRequest.getRespuestaCorrecta());
            examenRepository.save(examen.get());
            return examen.get();
        }
    }

    /**
     * @param examenId
     */
    @Override
    public void deleteExamen(Integer examenId) {
        if (examenRepository.getById(examenId).getId().equals(examenId)) {
            examenRepository.deleteById(examenId);
        }
    }

    /**
     * @param examenId
     * @return
     */
    @Override
    public Examen getExamenById(Integer examenId) {
        return examenRepository.findById(examenId).orElseThrow(() -> null);
    }

    /**
     * @return
     */
    @Override
    public List<Examen> getAllExamen() {
        return examenRepository.findAll();
    }
}
