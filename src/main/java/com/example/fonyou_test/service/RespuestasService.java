package com.example.fonyou_test.service;

import com.example.fonyou_test.data.models.Respuesta;
import com.example.fonyou_test.data.payloads.request.RespuestasRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RespuestasService {

    MessageResponse createRespuestas(RespuestasRequest respuestasRequest);

    Respuesta updateRespuestas(Integer respuestasId, RespuestasRequest respuestasRequest);

    void deleteRespuestas(Integer respuestasId);

    Respuesta getASingleRespuestas(Integer respuestasId);

    List<Respuesta> getAllRespuestas();
}
