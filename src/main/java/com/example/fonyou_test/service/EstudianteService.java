package com.example.fonyou_test.service;

import com.example.fonyou_test.data.models.Estudiante;
import com.example.fonyou_test.data.payloads.request.EstudianteRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface EstudianteService {

    MessageResponse createEstudiante(EstudianteRequest estudianteRequest);

    Estudiante updateEstudiante(Integer estudianteId, EstudianteRequest estudianteRequest);

    void deleteEstudiante(Integer estudianteId);

    Estudiante getASingleEstudiante(Integer estudianteId);

    List<Estudiante> getAllEstudiante();
}
