package com.example.fonyou_test.web;

import com.example.fonyou_test.data.models.Estudiante;
import com.example.fonyou_test.data.payloads.request.EstudianteRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import com.example.fonyou_test.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    @Autowired
    EstudianteService estudianteService;

    @GetMapping("/all")
    public ResponseEntity<List<Estudiante>> getAllEstudiantes() {
        List<Estudiante> estudiantes = estudianteService.getAllEstudiante();
        return new ResponseEntity<>(estudiantes, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Estudiante> getEstudiante(@PathVariable("id") Integer id) {
        Estudiante estudiante = estudianteService.getASingleEstudiante(id);
        return new ResponseEntity<>(estudiante, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addEstudiante(@RequestBody EstudianteRequest estudiante) {
        MessageResponse newEstudiante = estudianteService.createEstudiante(estudiante);
        return new ResponseEntity<>(newEstudiante, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Estudiante> updateEstudiante(@PathVariable Integer id, @RequestBody EstudianteRequest estudiante) {
        Estudiante updateEstudiante = estudianteService.updateEstudiante(id, estudiante);
        return new ResponseEntity<>(updateEstudiante, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteEstudiante(@PathVariable("id") Integer id) {
        estudianteService.deleteEstudiante(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
