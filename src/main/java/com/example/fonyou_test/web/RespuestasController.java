package com.example.fonyou_test.web;

import com.example.fonyou_test.data.models.Respuesta;
import com.example.fonyou_test.data.payloads.request.RespuestasRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import com.example.fonyou_test.service.RespuestasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/respuestas")
public class RespuestasController {

    @Autowired
    RespuestasService respuestasService;

    @GetMapping("/all")
    public ResponseEntity<List<Respuesta>> getAllRespuestas() {
        List<Respuesta> respuestas = respuestasService.getAllRespuestas();
        return new ResponseEntity<>(respuestas, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Respuesta> getRespuesta(@PathVariable("id") Integer id) {
        Respuesta respuesta = respuestasService.getASingleRespuestas(id);
        return new ResponseEntity<>(respuesta, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addRespuesta(@RequestBody RespuestasRequest respuestasRequest) {
        MessageResponse newRespuesta = respuestasService.createRespuestas(respuestasRequest);
        return new ResponseEntity<>(newRespuesta, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Respuesta> updateRespuesta(@PathVariable Integer id, @RequestBody RespuestasRequest respuestasRequest) {
        Respuesta updateRespuesta = respuestasService.updateRespuestas(id, respuestasRequest);
        return new ResponseEntity<>(updateRespuesta, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteRespuesta(@PathVariable("id") Integer id) {
        respuestasService.deleteRespuestas(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
