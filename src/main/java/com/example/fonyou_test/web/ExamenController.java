package com.example.fonyou_test.web;

import com.example.fonyou_test.data.models.Examen;
import com.example.fonyou_test.data.payloads.request.ExamenRequest;
import com.example.fonyou_test.data.payloads.response.MessageResponse;
import com.example.fonyou_test.service.ExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/examne")
public class ExamenController {

    @Autowired
    ExamenService examenService;

    @GetMapping("/all")
    public ResponseEntity<List<Examen>> getAllExamen() {
        List<Examen> examen = examenService.getAllExamen();
        return new ResponseEntity<>(examen, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Examen> getExamen(@PathVariable("id") Integer id) {
        Examen examen = examenService.getExamenById(id);
        return new ResponseEntity<>(examen, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addExamen(@RequestBody ExamenRequest examenRequest) {
        MessageResponse newExamen = examenService.createExamen(examenRequest);
        return new ResponseEntity<>(newExamen, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Examen> updateExamen(@PathVariable Integer id, @RequestBody ExamenRequest examenRequest) {
        Examen updateExamen = examenService.updateExamen(id, examenRequest);
        return new ResponseEntity<>(updateExamen, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteExamen(@PathVariable("id") Integer id) {
        examenService.deleteExamen(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
